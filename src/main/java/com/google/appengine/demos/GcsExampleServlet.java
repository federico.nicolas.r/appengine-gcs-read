/*
 * Copyright 2013 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.appengine.demos;


import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.sqladmin.SQLAdmin;
import com.google.api.services.sqladmin.model.ImportContext;
import com.google.api.services.sqladmin.model.InstancesImportRequest;
import com.google.api.services.sqladmin.model.Operation;
import com.google.api.services.sqladmin.model.ImportContext.CsvImportOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsInputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;

import org.apache.commons.logging.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.channels.Channels;
import java.security.GeneralSecurityException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;

@SuppressWarnings("serial")
public class GcsExampleServlet extends HttpServlet {

  @Override
  public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
    contar();

    String project = "os-profesionales-test";
    String instance = "profesionales-db";
    InstancesImportRequest requestBody = new InstancesImportRequest();
    ImportContext ic = new ImportContext();
    ic.setKind("sql#importContext");
    ic.setFileType("csv");
    ic.setUri("gs://os-profesionales-test.appspot.com/test.csv");
    ic.setDatabase(
        "jdbc:google:mysql://os-profesionales-test:us-central1:profesionales-db/profesionales?user=root&amp;password=root");
    CsvImportOptions csv = new CsvImportOptions();
    csv.setTable("profesional");
    List<String> list = new ArrayList<String>();
    list.add("value");
    csv.setColumns(list);
    ic.setCsvImportOptions(csv);
    requestBody.setImportContext(ic);
    SQLAdmin sqlAdminService = null;
    try {
      sqlAdminService = createSqlAdminService();
    } catch (GeneralSecurityException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    SQLAdmin.Instances.SQLAdminImport request = sqlAdminService.instances().sqladminImport(project, instance,
        requestBody);
    Operation response = request.execute();
    System.out.println("STATUSSSSSSSSSSSSS: " + response.getStatus());

    contar();

    /*
    String[] splits = req.getRequestURI().split("/", 4);
    String FILENAME = splits[3];
    
    GcsService gcsService = GcsServiceFactory.createGcsService();
    GcsFilename filename = new GcsFilename("os-profesionales-test.appspot.com", FILENAME);
    
    GcsInputChannel readChannel = null;
    BufferedReader reader = null;
    
    
    
    final String createTableSql = "CREATE TABLE IF NOT EXISTS profesional ( id INT NOT NULL AUTO_INCREMENT, value VARCHAR(200) NOT NULL, PRIMARY KEY (id) )";
    
    final String createProfesionalSQL = "INSERT INTO profesional (value) VALUES (?)";
    
    final String selectProfesionalSQL = "SELECT COUNT(*) as TOTAL from profesional";
    
    String url;
    if (System.getProperty("com.google.appengine.runtime.version").startsWith("Google App Engine/")) {
      url = System.getProperty("ae-cloudsql.cloudsql-database-url");
      log("URL interno: " + url);
      try {
        Class.forName("com.mysql.jdbc.GoogleDriver");
        log("google driver");
      } catch (ClassNotFoundException e) {
        log("ClassNotFoundException");
        throw new ServletException("Error loading Google JDBC Driver", e);
      }
    } else {
      url = System.getProperty("ae-cloudsql.local-database-url");
      log("URL local: " + url);
    }
    log("connecting to: " + url);
    
    try (Connection conn = DriverManager.getConnection(url);
      PreparedStatement statementCreateProfesional = conn.prepareStatement(createProfesionalSQL)) {
        conn.createStatement().executeUpdate(createTableSql);
    
        try (ResultSet rs = conn.prepareStatement(selectProfesionalSQL).executeQuery()) {
          while (rs.next()) {
            String total = rs.getString("TOTAL");
            log("TOTAL: " + total);
          }
        }
    
        try {
          readChannel = gcsService.openReadChannel(filename, 0);
          reader = new BufferedReader(Channels.newReader(readChannel, "UTF8"));
          String line;
          while ((line = reader.readLine()) != null) {
            statementCreateProfesional.setString(1, line);
            statementCreateProfesional.executeUpdate();
          }
        } finally {
          if (reader != null) { reader.close(); } 
        }
        
        try (ResultSet rs = conn.prepareStatement(selectProfesionalSQL).executeQuery()) {
          while (rs.next()) {
            String total = rs.getString("TOTAL");
            log("TOTAL: " + total);
          }
        }
    
    } catch (SQLException e) {
      throw new ServletException("SQL error", e);
    }
    
    */
  }
  
  public void contar() throws ServletException {

    final String selectProfesionalSQL = "SELECT COUNT(*) as TOTAL from profesional";

    String url;
    if (System.getProperty("com.google.appengine.runtime.version").startsWith("Google App Engine/")) {
      url = System.getProperty("ae-cloudsql.cloudsql-database-url");
      log("URL interno: " + url);
      try {
        Class.forName("com.mysql.jdbc.GoogleDriver");
        log("google driver");
      } catch (ClassNotFoundException e) {
        log("ClassNotFoundException");
        throw new ServletException("Error loading Google JDBC Driver", e);
      }
    } else {
      url = System.getProperty("ae-cloudsql.local-database-url");
      log("URL local: " + url);
    }
    log("connecting to: " + url);

    try (Connection conn = DriverManager.getConnection(url)) {
      try (ResultSet rs = conn.prepareStatement(selectProfesionalSQL).executeQuery()) {
        while (rs.next()) {
          String total = rs.getString("TOTAL");
          log("TOTAL: " + total);
        }
      }

    } catch (SQLException e) {
      throw new ServletException("SQL error", e);
    }
  }

  public static SQLAdmin createSqlAdminService() throws IOException, GeneralSecurityException {
    HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
    JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();

    GoogleCredential credential = GoogleCredential.getApplicationDefault();
    if (credential.createScopedRequired()) {
      credential = credential.createScoped(Arrays.asList("https://www.googleapis.com/auth/cloud-platform"));
    }

    return new SQLAdmin.Builder(httpTransport, jsonFactory, credential).setApplicationName("Google-SQLAdminSample/0.1")
        .build();
  }

}  